
/* ---- /1NHXyHdJimWTCcYByRSM7RGVfUkixwjf41/js/ZeroFrame.js ---- */


// Version 1.0.0 - Initial release
// Version 1.1.0 (2017-08-02) - Added cmdp function that returns promise instead of using callback
// Version 1.2.0 (2017-08-02) - Added Ajax monkey patch to emulate XMLHttpRequest over ZeroFrame API
// Version 1.3.0 (2018-12-05) - Added monkey patch for fetch API

const CMD_INNER_READY = 'innerReady'
const CMD_RESPONSE = 'response'
const CMD_WRAPPER_READY = 'wrapperReady'
const CMD_PING = 'ping'
const CMD_PONG = 'pong'
const CMD_WRAPPER_OPENED_WEBSOCKET = 'wrapperOpenedWebsocket'
const CMD_WRAPPER_CLOSE_WEBSOCKET = 'wrapperClosedWebsocket'

class ZeroFrame {
    constructor(url) {
        this.url = url
        this.waiting_cb = {}
        this.wrapper_nonce = document.location.href.replace(/.*wrapper_nonce=([A-Za-z0-9]+).*/, "$1")
        this.connect()
        this.next_message_id = 1
        this.init()
    }

    init() {
        return this
    }

    connect() {
        this.target = window.parent
        window.addEventListener('message', e => this.onMessage(e), false)
        this.cmd(CMD_INNER_READY)
    }

    onMessage(e) {
        let message = e.data
        let cmd = message.cmd
        if (cmd === CMD_RESPONSE) {
            if (this.waiting_cb[message.to] !== undefined) {
                this.waiting_cb[message.to](message.result)
            }
            else {
                this.log("Websocket callback not found:", message)
            }
        } else if (cmd === CMD_WRAPPER_READY) {
            this.cmd(CMD_INNER_READY)
        } else if (cmd === CMD_PING) {
            this.response(message.id, CMD_PONG)
        } else if (cmd === CMD_WRAPPER_OPENED_WEBSOCKET) {
            this.onOpenWebsocket()
        } else if (cmd === CMD_WRAPPER_CLOSE_WEBSOCKET) {
            this.onCloseWebsocket()
        } else {
            this.onRequest(cmd, message)
        }
    }

    onRequest(cmd, message) {
        this.log("Unknown request", message)
    }

    response(to, result) {
        this.send({
            cmd: CMD_RESPONSE,
            to: to,
            result: result
        })
    }

    cmd(cmd, params={}, cb=null) {
        this.send({
            cmd: cmd,
            params: params
        }, cb)
    }

    cmdp(cmd, params={}) {
        return new Promise((resolve, reject) => {
            this.cmd(cmd, params, (res) => {
                if (res && res.error) {
                    reject(res.error)
                } else {
                    resolve(res)
                }
            })
        })
    }

    send(message, cb=null) {
        message.wrapper_nonce = this.wrapper_nonce
        message.id = this.next_message_id
        this.next_message_id++
        this.target.postMessage(message, '*')
        if (cb) {
            this.waiting_cb[message.id] = cb
        }
    }

    log(...args) {
        console.log.apply(console, ['[ZeroFrame]'].concat(args))
    }

    onOpenWebsocket() {
        this.log('Websocket open')
    }

    onCloseWebsocket() {
        this.log('Websocket close')
    }

    monkeyPatchAjax() {
        var page = this
        XMLHttpRequest.prototype.realOpen = XMLHttpRequest.prototype.open
        this.cmd("wrapperGetAjaxKey", [], (res) => { this.ajax_key = res })
        var newOpen = function (method, url, async) {
            url += "?ajax_key=" + page.ajax_key
            return this.realOpen(method, url, async)
        }
        XMLHttpRequest.prototype.open = newOpen

        window.realFetch = window.fetch
        var newFetch = function (url) {
            url += "?ajax_key=" + page.ajax_key
            return window.realFetch(url)
        }
        window.fetch = newFetch
    }
}

/* ---- /1NHXyHdJimWTCcYByRSM7RGVfUkixwjf41/js/page.js ---- */


VERSION = 0.2;

datazite = "1D2xzWx2oG8oUiMpKvygeoyYL7uJXFMp8k";
datalist = null;
thumbnails = null;

getThumbnails = function(cb) {
    corsCmd(function() {
        page.cmd("fileGet", {
            "inner_path": "cors-" + datazite + "/data/thumbnails.json",
            "required": true
        }, (_thumbnails) => {
            if (_thumbnails) {
                thumbnails = JSON.parse(_thumbnails);

                typeof cb === "function" && cb();
            }
        });
    });
};

getDatalist = function(cb) {
    datalist = [];
    getThumbnails(function() {
        for (let name in thumbnails) {
            datalist.push(name);
        }

        typeof cb === "function" && cb(datalist);
    });
};

requestDatafileLoad = function(name, cb) {
    let filepath = "cors-" + datazite + "/" + "data/images/" + name;

    console.log("datafile load requesting..", filepath);

    corsCmd(function() {
        page.cmd("fileGet", [filepath, true, "base64", 1000], (res) => {
            console.log("datafile load request", res, filepath);

            typeof cb === "function" && cb();
        });
    });
};

requestDatafileDelete = function(name) {
    corsCmd(function() {
        page.cmd("wrapperOpenWindow", ["/" + datazite + "/deleter.html?viewzite=" + page.site_info.address + "&filename=" + name, "_self"]);
    });
};

checkDatafileInfo = function(name, img, btn_load, btn_delete) {
    let filepath = "data/images/" + name;

    corsCmd(function() {
        page.cmd("as", [datazite, "optionalFileInfo", [filepath]], (res) => {
            if (!res) {
                res = {
                    "is_downloaded": false
                };
            }

            console.log("got optional file info", name, res);

            img.src = res.is_downloaded ? "cors-" + datazite + "/data/images/" + name : thumbnails[name];

            btn_load.style.display = !res.is_downloaded ? "" : "none";
            btn_delete.style.display = res.is_downloaded ? "" : "none";
        });
    });
};

renderListItem = function(name) {
    let el = document.createElement("div");
    el.classList.add("list-item");

    el.appendChild(document.createTextNode(name));

    let img = document.createElement("img");
    img.src = thumbnails[name];
    el.appendChild(img);

    let btn_info = document.createElement("button");
    btn_info.innerHTML = "Get info";
    btn_info.onclick = function() {
        checkDatafileInfo(name, img, btn_load, btn_delete);
    };
    el.appendChild(btn_info);

    let btn_load = document.createElement("button");
    btn_load.innerHTML = "Load image";
    btn_load.onclick = function() {
        requestDatafileLoad(name, function() {
            checkDatafileInfo(name, img, btn_load, btn_delete);
        });
    };
    el.appendChild(btn_load);

    let btn_delete = document.createElement("button");
    btn_delete.innerHTML = "Delete image";
    btn_delete.onclick = function() {
        requestDatafileDelete(name, function() {
            checkDatafileInfo(name, img, btn_load, btn_delete);
        });
    };
    el.appendChild(btn_delete);

    checkDatafileInfo(name, img, btn_load, btn_delete);

    renderlist.appendChild(el);
};

renderList = function() {
    renderlist.innerHTML = "";

    for (let dliI = 0; dliI < datalist.length; dliI++) {
        let dli = datalist[dliI];
        renderListItem(dli);
    }
};

corsCmd = function(cb) {
    if (page.site_info.settings.permissions.indexOf("Cors:" + datazite) < 0) {
        // no permission yet granted
        page.cmd("corsPermission", [datazite], () => {
            // permission granted
            typeof cb === "function" && cb();
        });
    } else {
        // already got permission
        typeof cb === "function" && cb();
    }
};

class Page extends ZeroFrame {
    setSiteInfo(site_info) {
        var out = document.getElementById("out");
        page.site_info = site_info;
        out.innerHTML =
            "Page address: " + site_info.address +
            "<br>- Zite Version: " + VERSION +
            "<br>- Peers: " + site_info.peers +
            "<br>- Size: " + site_info.settings.size +
            "<br>- Modified: " + (new Date(site_info.content.modified * 1000));
    }

    onOpenWebsocket() {
        this.cmd("siteInfo", [], function(site_info) {
            page.setSiteInfo(site_info);

            getDatalist(function() {
                renderList();
            });
        });
    }

    onRequest(cmd, message) {
        if (cmd == "setSiteInfo") {
            this.setSiteInfo(message.params);
        } else {
            this.log("Unknown incoming message:", cmd);
        }
    }
}
page = new Page();

btn_getDatalist = document.createElement("button");
btn_getDatalist.appendChild(document.createTextNode("Reload"));
btn_getDatalist.onclick = function() {
    getDatalist(function() {
        renderList();
    });
};
buttons.appendChild(btn_getDatalist);