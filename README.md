# Sample Image View Zite

Has to be used in combination with a [Sample Image Data Zite](https://gitlab.com/AnthyG/sample-image-data-zite).

To do so, change `datazite` in `js/page.js` to the address of that image data zite.

Clone this repo into a new zite, edit the `ignore` property in it's `content.json` to look like follows:

`"ignore": "((js|css)/(?!all.(js|css))|.git)"`